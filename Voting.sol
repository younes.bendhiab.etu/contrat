// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Voting is Ownable (msg.sender) {
    mapping(address => Voter) public voters;
    Proposal[] public proposals;
    WorkflowStatus public workflowStatus;
    address[] public allVoters;
    uint public quorumRequirement;

    constructor() {
        quorumRequirement = 10;
    }

    function setQuorumRequirement(uint _quorum) public onlyOwner {
        quorumRequirement = _quorum;
    }

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
    }

    struct Proposal {
        string description;
        uint voteCount;
    }

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(
        WorkflowStatus previousStatus,
        WorkflowStatus newStatus
    );
    event ProposalRegistered(uint proposalId);
    event Voted(address voter, uint proposalId);
    event VoteCancelled(address voter, uint proposalId);
    event ResultsBroadcast();
    event VoteConfirmation(address voter, uint proposalId);

    function registerVoter(address _voterAddress) public onlyOwner {
        require(
            workflowStatus == WorkflowStatus.RegisteringVoters,
            "Impossible d'inscrire les electeurs pour le moment."
        );
        require(
            !voters[_voterAddress].isRegistered,
            "L'electeur est deja enregistree"
        );

        voters[_voterAddress] = Voter({
            isRegistered: true,
            hasVoted: false,
            votedProposalId: 0
        });

        allVoters.push(_voterAddress);

        emit VoterRegistered(_voterAddress);
    }

    function startProposalsRegistration() public onlyOwner {
        workflowStatus = WorkflowStatus.ProposalsRegistrationStarted;

        emit WorkflowStatusChange(
            WorkflowStatus.RegisteringVoters,
            workflowStatus
        );
    }

    function registerProposal(string memory _description) public {
        require(
            workflowStatus == WorkflowStatus.ProposalsRegistrationStarted,
            "Impossible d'enregistrer des propositions pour le moment."
        );
        require(
            voters[msg.sender].isRegistered,
            "Seuls les electeurs inscrits peuvent soumettre des propositions."
        );

        proposals.push(Proposal({description: _description, voteCount: 0}));

        emit ProposalRegistered(proposals.length - 1);
    }

    function startVotingSession() public onlyOwner {
        workflowStatus = WorkflowStatus.VotingSessionStarted;

        emit WorkflowStatusChange(
            WorkflowStatus.ProposalsRegistrationEnded,
            workflowStatus
        );
    }

    function vote(uint _proposalId) public {
        require(
            workflowStatus == WorkflowStatus.VotingSessionStarted,
            "Impossible de voter pour le moment"
        );
        require(
            voters[msg.sender].isRegistered,
            "Seulement les electeurs enregistrees peuvent voter"
        );
        require(!voters[msg.sender].hasVoted, "L'electeur a deja votee");
        require(_proposalId < proposals.length, "ID non valide");

        proposals[_proposalId].voteCount++;
        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = _proposalId;

        proposals[_proposalId].voteCount++;

        emit Voted(msg.sender, _proposalId);
        emit VoteConfirmation(msg.sender, _proposalId);
    }

    function endVotingSession() public onlyOwner {
        workflowStatus = WorkflowStatus.VotesTallied;

        emit WorkflowStatusChange(
            WorkflowStatus.VotingSessionEnded,
            workflowStatus
        );
        emit ResultsBroadcast();
    }

    function getWinner() public view returns (uint winningProposalId) {
        require(
            workflowStatus == WorkflowStatus.VotesTallied,
            "Le gagnant ne peut pas etre choisis pour le moment"
        );

        uint winningVoteCount = 0;
        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > winningVoteCount) {
                winningVoteCount = proposals[i].voteCount;
                winningProposalId = i;
            }
        }
    }

    function getAllVoters() public view returns (address[] memory) {
        return allVoters;
    }

    // Permet d'annuler un vote
    function cancelVote() public {
        require(
            workflowStatus == WorkflowStatus.VotingSessionStarted,
            "Ce vote ne peut pas etre annulee pour le moment."
        );
        require(voters[msg.sender].hasVoted, "L'electeur n'a pas encore votee");

        uint proposalId = voters[msg.sender].votedProposalId;
        voters[msg.sender].hasVoted = false;
        voters[msg.sender].votedProposalId = 0;
        proposals[proposalId].voteCount--;

        emit VoteCancelled(msg.sender, proposalId);
    } 
}