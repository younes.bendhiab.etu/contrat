# Smart Contract
## Fonctionnalités ajoutées

Annulation de vote (fonction cancelVote()): <br>
Permet aux électeurs d'annuler leur vote.

Affichage des résultats des votes pour les participants: <br>
Diffuse les résultats de l'élection à tous les participants

Affichage de tout les participants (fonction getAllVoters()): <br>
Renvoie un tableau des adresses de tous les électeurs ayant votés.